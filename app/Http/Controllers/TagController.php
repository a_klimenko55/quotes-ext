<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Resources\Tag as TagResource;


class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        //Get tags
        $tags = Tag::all();

        return TagResource::collection($tags);
    }

    /**
     * Search for tags by name.
     *
     * @param string $term
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function search(Request $request)
    {
        $term = $request->get('term');
        //Search tags
        $tags = Tag::query()->where('text', 'LIKE', "%{$term}%")->get();

        return TagResource::collection($tags);
    }
}
