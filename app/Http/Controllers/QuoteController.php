<?php

namespace App\Http\Controllers;

use App\Models\Quote;
use App\Http\Resources\Quote as QuoteResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

class QuoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {

        // Get articles
        $quotes = Quote::with('tags')->orderBy('created_at', 'desc')->paginate(10);

        // Return collection of articles as a resource
        return QuoteResource::collection($quotes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return QuoteResource
     */
    public function store(Request $request)
    {
        $quote = new Quote();
        $quote->author = $request->json('author');
        $quote->text = $request->json('text');
        if ($quote->save()) {
            $tags = $request->json('tags');
            foreach ($tags as $tag) {
                DB::table('quote_tags')->insert([
                    'quote_id' => $quote->id,
                    'tag_id' => $tag['id']
                ]);
            }
            return new QuoteResource($quote);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return QuoteResource|NotFoundResourceException
     */
    public function update(Request $request, int $id)
    {
        $quote = Quote::find($id);
        if ($quote) {
            $quote->author = $request->json('author');
            $quote->text = $request->json('text');
            if ($quote->save()) {
                $tags = $request->json('tags');
                $rows = array_map(fn($tag) => ['quote_id' => $quote->id, 'tag_id' => $tag['id']], $tags);
                DB::table('quote_tags')->whereNotIn('tag_id', array_column($rows, 'tag_id'))
                    ->where('quote_id', '=', $quote->id)
                    ->delete();
                DB::table('quote_tags')->upsert($rows, ['quote_id', 'tag_id']);
                return new QuoteResource($quote);
            }
        } else {
            return new NotFoundResourceException();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return QuoteResource|NotFoundResourceException
     */
    public function destroy(int $id)
    {
        $quote = Quote::query()->findOrFail($id);
        if ($quote->delete()) {
            return new QuoteResource($quote);
        } else {
            return new NotFoundResourceException();
        }
    }
}
