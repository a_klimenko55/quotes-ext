<?php

use App\Http\Controllers\TagController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\QuoteController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// List of quotes
Route::get('quotes', [QuoteController::class, 'index']);

// Create new quote
Route::post('quote', [QuoteController::class, 'store']);

// Update quote
Route::put('quote/{id}', [QuoteController::class, 'update']);

// Delete quote
Route::delete('quote/{id}', [QuoteController::class, 'destroy']);

// List of tags
Route::get('tags', [TagController::class, 'index']);

// Search tags
Route::get('tags/search', [TagController::class, 'search']);
